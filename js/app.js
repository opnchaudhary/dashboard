var nosk = angular.module('nosk',[]);

nosk.config(function($routeProvider) {
	$routeProvider.when('/repo/:repo/:owner',{templateUrl:'contributor.html',controller:repoCtrl})
});

function mainCtrl($scope, $rootScope, $http, $route, $location, $routeParams){
	$http.jsonp('https://api.github.com/orgs/awecode/repos?callback=JSON_CALLBACK').success(function(data){
		$rootScope.repos = data.data;				
		for (var i=0;i<data.data.length;i++){
			/*$http.jsonp('http://urls.api.twitter.com/1/urls/count.json?url='+$rootScope.repos[i].html_url+'&callback=JSON_CALLBACK').success(function(data){
				x=data.count;	
				$scope.tweet=x;				
				//$rootScope.repos[i].tweet=String(data.count);					
			});*/
			$rootScope.repos[i].tweet=i;			
		}
		/*var count=0;
		foreach(rep as data.data){
			$http.jsonp('https://api.github.com/repos/foss-np/miti/contributors?callback=JSON_CALLBACK').success(function(data){
				$rootScope.repos[count].contributors=data.data;
			});		
			count=count+1;
		}*/
	});		
	$http.jsonp('https://api.github.com/repos/foss-np/miti/contributors?callback=JSON_CALLBACK').success(function(data){
		$scope.contributors=data.data;
	});
	
	$scope.$route = $route;
	$scope.$location = $location;
	$scope.$routeParams = $routeParams;
}

function repoCtrl($scope, $http, $routeParams){
	$http.jsonp('https://api.github.com/repos/'+$routeParams.owner+'/'+$routeParams.repo+'/contributors?callback=JSON_CALLBACK').success(function(data){
		$scope.contributors = data.data;
	});
}
