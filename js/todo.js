var todoApp=angular.module('todoApp',[]);

todoApp.controller('todoCtrl',function todoCtrl($scope){
	$scope.username='cpNath';
	
	var tasks=[
		{id:1,text:'task1',done:true},
		];	

	var priority=[
			{text:'high',value:1},
			{text:'medium',value:2},
			{text:'low',value:3},
			];
	$scope.tasks=tasks;
	
	$scope.remaining=function(){
		return tasks.reduce(function(count,task){
			return task.done?count:count+1;
		},0);
	};
	
	$scope.addTask=function(newTask){
		var task={id:tasks.length+1,text:newTask.text,done:false};
		tasks.push(task);
		newTask.text='';
	};
	
	$scope.archive=function(){
		tasks=$scope.tasks=tasks.filter(function(){
					return tasks.done;
					});
	};

	
});
